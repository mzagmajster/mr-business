# Mr. Business (Feedback)

In general Mr. Business is the cover name and it should get more applications later, but we will se 
how it goes 


## Getting Started

### Prerequisites

Install Python 3, I strongly suggest isolating the environment from your base operating system in 
some way (for example by using virtualenv).


### Installing

Install requirements.

```
pip install -r requiremnets.txt
```

Copy ```.env.example``` to ```.env``` and adjust the settings.

```
cp .env.example .env
```

Make sure your database is fully migrated.

```
python manage.py migrate
```

Run the server and check if application runs.

```
python manage.py runserver
```

## Running the tests

[todo]

## Deployment

[todo]

## Usage

We have to register each proposal we would like to get any feedback by running. What the above 
command esentially does is create a link potential client/employer can visit. Sequence number 
here is unique identifier of proposal (typically cover letter + CV)

```
python manage.py register <sequence_number>
```

## Changelog

[todo]

## Documentation

[todo]

## Built With

* [Python](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Django](https://maven.apache.org/) - Dependency Management

## Contributing

[todo]

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository. 

## Authors

Initial content for this project was provided by [Matic Zagmajster](http://maticzagmajster.ddns.net/). For more information please see ```AUTHORS``` file.

## License

This project is licensed under the MIT License - see the ```LICENSE``` file for details.

