from django.forms import Form, CharField, Textarea


class FeedbackForm(Form):
	feedback = CharField(widget=Textarea())