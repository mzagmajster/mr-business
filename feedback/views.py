from datetime import datetime
from django.shortcuts import render, redirect

from feedback.models import Proposal
from feedback.forms import FeedbackForm


def index(request, hmac, token):
	valid_request = True
	p = Proposal.objects.filter(token=token).first()
	form = FeedbackForm()

	if p is None or not p.compare_hmac_hex(hmac) or p.submitted_at is not None:
		valid_request = False

	if request.method == 'POST':
		form = FeedbackForm(request.POST)

		if not form.is_valid():
			valid_request = False
		else:
			p.feedback = form.cleaned_data['feedback']
			p.submitted_at = datetime.utcnow()
			p.save()
			return redirect('feedback:feedback-thank-you')

	return render(request, 'feedback/index.html', {
		'form': form,
		'valid_request': valid_request,
		'hmac': hmac,
		'token': token
	})


def thank_you(request):
	return render(request, 'feedback/thank-you.html', {})