from uuid import uuid4
from hashlib import sha512
import hmac, datetime
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from feedback.models import Proposal


class Command(BaseCommand):
	help = "Register propsal for feedback."

	def add_arguments(self, parser):
		parser.add_argument('sn', type=int)

	def handle(self, *args, **options):
		sn = options['sn']
		try:
			Proposal.objects.get(sequence_number=sn)
		except Proposal.DoesNotExist:
			pass
		else:
			raise CommandError("Record for this propsal already exists.")

		token = uuid4().hex
		p = Proposal(
			sequence_number=sn, 
			token=token
		)
		p.save()

		self.stdout.write("Link: %s" % p.generate_link())
