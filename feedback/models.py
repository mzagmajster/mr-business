from uuid import uuid4
from hashlib import sha512
import hmac, datetime
from django.conf import settings
from django.db.models import Model, DateTimeField, IntegerField, CharField, TextField, BooleanField


class Proposal(Model):
	created_at 		= DateTimeField(auto_now_add=True)
	modified_at		= DateTimeField(auto_now=True, blank=True)

	sequence_number = IntegerField()
	token			= CharField(max_length=255, unique=True)
	feedback		= TextField(blank=True)
	viewed 			= BooleanField(default=False)

	submitted_at 	= DateTimeField(null=True)

	def get_hmac_hex(self):
		m = '#'.join([
			self.created_at.strftime("%Y-%m-%d, %H:%M:%S"),
			self.token,
			str(self.sequence_number)
		])
		h = hmac.new(settings.PROPOSAL_SECRET_KEY, m.encode('utf8'), sha512)
		return h.hexdigest()

	def generate_link(self):
		l = "/feedback/" + self.get_hmac_hex() + "/" + self.token
		return l

	def compare_hmac_hex(self, request_hmac):
		return hmac.compare_digest(self.get_hmac_hex(), request_hmac)


